//
//  JobsTableViewController.swift
//  SuchosApp
//
//  Created by Tom Odler on 07.02.18.
//  Copyright © 2018 Tom Odler. All rights reserved.
//

import UIKit
import CoreData

protocol JobSelectionDelegate: class {
    func jobSelected(_ newJob: Job)
}

class JobsTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {

    var jobs : NSFetchedResultsController<Job>?
    var selectedJobDetailVC : JobDetailVC?{
        didSet{
            if(jobs!.fetchedObjects!.count > 0){
                showDetailOfJob(newJob: jobs!.fetchedObjects!.first!)
            }
        }
    }
    
    weak var delegate : JobSelectionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
         self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        let exitBtn = UIBarButtonItem.init(title: "Back", style: .plain, target: self, action: #selector(self.exitSettings))
        self.navigationItem.leftBarButtonItem = exitBtn
        fetchJobs()
    }
    
    @objc func exitSettings(){
        self.splitViewController?.dismiss(animated: true, completion: nil)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        guard let jobs = jobs?.fetchedObjects else{
            if self.isEditing{
                return 1
            } else {
               return 0
            }
            
        }
        if self.isEditing {
            return jobs.count+1
        } else {
            return jobs.count
        }
        
    }
    
    func fetchJobs(){
        let context = CoreDataManager.sharedManager.context
        
        if let context = context{
            let fRequest = NSFetchRequest<Job>.init(entityName: "Job")
            let descriptor = NSSortDescriptor.init(key: "name", ascending: true)
            fRequest.sortDescriptors = [descriptor]
            jobs = NSFetchedResultsController(fetchRequest: fRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
            jobs!.delegate = self
            do {
                try jobs!.performFetch()
                print("Number of jobs: \(jobs!.fetchedObjects!.count)")
                if jobs!.fetchedObjects!.count == 0 {
                    _ = CoreDataManager.sharedManager.createJob()
                }
                
            } catch {
                print("nepovedlo se načíst elany! \(error)")
            }
        }
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mCell", for: indexPath)
        
       configureCell(indexPath: indexPath, cell: cell)

        // Configure the cell...
        return cell
    }
    
    func configureCell(indexPath: IndexPath, cell: UITableViewCell){
        if(self.isEditing && indexPath.row == jobs!.fetchedObjects?.count){
            cell.textLabel?.text = "Add new"
        } else {
            let job = jobs!.object(at: indexPath)
            
            cell.textLabel?.text = job.name
        }
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        
        let indexPath : IndexPath!
        
        if let mJobs = jobs?.fetchedObjects {
            indexPath = IndexPath.init(row: mJobs.count, section: 0)
        } else {
            indexPath = IndexPath.init(row: 0, section: 0)
        }
        if editing {
            self.tableView.insertRows(at: [indexPath], with: .automatic)
        } else {
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
 

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            let job = jobs?.object(at: indexPath)
            jobDeleted(deletedJob: job!)
            CoreDataManager.sharedManager.context?.delete(job!)
            CoreDataManager.sharedManager.saveContext()
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
            _ = CoreDataManager.sharedManager.createJob()
            
        }    
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.isEditing {
            if indexPath.row == jobs!.fetchedObjects?.count{
                _ = CoreDataManager.sharedManager.createJob()
            } else {
                guard let job = jobs?.object(at: indexPath) else {
                    return
                }
                
                jobDeleted(deletedJob: job)
                CoreDataManager.sharedManager.context?.delete(job)
                CoreDataManager.sharedManager.saveContext()
            }
        }
        
        guard let job = jobs?.object(at: indexPath) else {
            return
        }
        showDetailOfJob(newJob: job)
    }
    
    func showDetailOfJob(newJob : Job){
        if let detailController = selectedJobDetailVC{
            detailController.jobSelected(newJob)
        } else {
            print("Jeste nemam detail")
        }
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        if(indexPath.row == jobs?.fetchedObjects?.count){
            return .insert
        } else {
            return .delete
        }
        
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .move:
            if let indexPath = indexPath{
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let indexPath = newIndexPath{
                tableView.insertRows(at: [indexPath], with: .fade)
            }
            break
        case .delete:
            if let indexPath = indexPath{
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break
        case .insert:
            if let indexPath = newIndexPath{
                tableView.insertRows(at: [indexPath], with: .fade)
            }
            break
        case .update:
            if let indexPath = indexPath{
                let cell = tableView.cellForRow(at: indexPath)
                configureCell(indexPath: indexPath, cell: cell!)
            }
            break
        }
    }
    
    func jobDeleted(deletedJob : Job){
        if jobs!.fetchedObjects!.isEmpty == true {
            let job = CoreDataManager.sharedManager.createJob()
            showDetailOfJob(newJob: job)
        } else {
            if deletedJob == selectedJobDetailVC?.mJob!{
                let job = jobs!.fetchedObjects!.first
                showDetailOfJob(newJob: job!)
            }
        }
    }
}
