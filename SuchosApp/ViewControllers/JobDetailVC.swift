//
//  JobDetailVC.swift
//  SuchosApp
//
//  Created by Tom Odler on 07.02.18.
//  Copyright © 2018 Tom Odler. All rights reserved.
//

import UIKit

class JobDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource, JobSelectionDelegate, UITextFieldDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    private var rowStrings = ["Name", "Price"]
    private var actualTf : UITextField?
    var mJob : Job? {
        didSet{
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.register(UINib.init(nibName: "JobDetailTbvCell", bundle: nil), forCellReuseIdentifier: "mCell")
        
//        let barBtn = UIBarButtonItem.init(title: "Save", style: .plain, target: self, action: #selector(JobDetailVC.saveDetails))
//        self.navigationItem.rightBarButtonItem = barBtn
        
        let navController = self.splitViewController?.viewControllers.first as! UINavigationController
        let masterController = navController.viewControllers.first as! JobsTableViewController
        masterController.selectedJobDetailVC = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let tf = actualTf {
            tf.resignFirstResponder()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowStrings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mCell") as! JobDetailTbvCell
        
        if let job = mJob{
            cell.nameLbl.text = rowStrings[indexPath.row]
            cell.valueTf.delegate = self
            switch indexPath.row {
            case 0:
                cell.valueTf.text = job.name
                cell.valueTf.keyboardType = .default
                cell.valueTf.returnKeyType = .next
                break
            case 1:
                cell.valueTf.text = "\(job.price)"
                cell.valueTf.keyboardType = .decimalPad
                cell.valueTf.returnKeyType = .done
                break
                
            default:
                cell.valueTf.text = "N/A"
                break;
            }
            
        }
        
        cell.valueTf.tag = indexPath.row
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! JobDetailTbvCell
        cell.valueTf.becomeFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    @objc func saveDetails(){
        if let job = mJob{
            let nameCell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! JobDetailTbvCell
            let priceCell = tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! JobDetailTbvCell
            
            guard let newName = nameCell.valueTf.text else {
                return
            }
            
            guard var newPrice = priceCell.valueTf.text else {
                return
            }
            
            newPrice = newPrice.replacingOccurrences(of: ",", with: ".")
            
            guard let mPrice = newPrice.doubleValue else {
                return
            }
            
            job.name = newName
            job.price = mPrice
            CoreDataManager.sharedManager.saveContext()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        actualTf = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        saveDetails()
        actualTf = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 0 {
            let cell = tableView.cellForRow(at: IndexPath.init(row: 1, section: 0)) as! JobDetailTbvCell
            cell.valueTf.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
    
    func jobSelected(_ newJob: Job) {
        mJob = newJob
    }
}


extension String {
    var doubleValue: Double? {
        return Double(self)
    }
    var floatValue: Float? {
        return Float(self)
    }
    var integerValue: Int? {
        return Int(self)
    }
}
