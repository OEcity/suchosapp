//
//  ViewController.swift
//  SuchosApp
//
//  Created by Tom Odler on 07.02.18.
//  Copyright © 2018 Tom Odler. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate {
    @IBOutlet weak var leftTbv: UITableView!
    @IBOutlet weak var rightTbv: UITableView!
    
    var leftTbvJobs : NSFetchedResultsController<Job>?
    var righTbvJobs : NSFetchedResultsController<Job>?
    
    @IBOutlet weak var sumView: UIView!
    
    @IBOutlet weak var totalValueLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let nib = UINib.init(nibName: "MainViewTbvCell", bundle: nil)
        leftTbv.register(nib, forCellReuseIdentifier: "mCell")
        rightTbv.register(nib, forCellReuseIdentifier: "mCell")
        
        leftTbv.setEditing(true, animated: false)
        rightTbv.setEditing(true, animated: false)
        
        initFetchers()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == leftTbv {
            guard let jobs = leftTbvJobs else {
                return 0
            }
            return jobs.fetchedObjects!.count
        } else {
            guard let jobs = righTbvJobs else {
                return 0
            }
            return jobs.fetchedObjects!.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mCell", for: indexPath) as? MainViewTbvCell
        
        configureCell(cell: cell!, indexPath: indexPath, tableView: tableView)
        
        cell?.selectionStyle = .none
        return cell!
    }
    
    func configureCell(cell : MainViewTbvCell, indexPath : IndexPath, tableView: UITableView){
        var jobForRow : Job!
        if tableView == leftTbv {
            jobForRow = leftTbvJobs!.fetchedObjects![indexPath.row]
            
        } else {
            jobForRow = righTbvJobs!.fetchedObjects![indexPath.row]
        }
        
        cell.nameLbl.text = jobForRow.name
        cell.priceLbl.text = "\(jobForRow.price),-"
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func initFetchers(){
        let context = CoreDataManager.sharedManager.context
        
        if let context = context{
            let fRequest = NSFetchRequest<Job>.init(entityName: "Job")
            let predicate = NSPredicate.init(format: "self.isSelected == false")
            
            let fRequest2 = NSFetchRequest<Job>.init(entityName: "Job")
            let predicate2 = NSPredicate.init(format: "self.isSelected == true")
            
            let descriptor = NSSortDescriptor.init(key: "name", ascending: true)
            fRequest.sortDescriptors = [descriptor]
            fRequest.predicate = predicate
            
            fRequest2.sortDescriptors = [descriptor]
            fRequest2.predicate = predicate2
            
            leftTbvJobs = NSFetchedResultsController(fetchRequest: fRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
            leftTbvJobs!.delegate = self
            do {
                try leftTbvJobs!.performFetch()
                print("Number of unselected jobs: \(leftTbvJobs!.fetchedObjects!.count)")
                
            } catch {
                print("nepovedlo se načíst unselected jobs! \(error)")
            }
            
            righTbvJobs = NSFetchedResultsController(fetchRequest: fRequest2, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
            righTbvJobs!.delegate = self
            do {
                try righTbvJobs!.performFetch()
                print("Number of selected jobs: \(righTbvJobs!.fetchedObjects!.count)")
                sumJobsPrice()
                
            } catch {
                print("nepovedlo se načíst selected jobs! \(error)")
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let job : Job?
        if tableView == leftTbv {
            job = leftTbvJobs?.object(at: indexPath)
        } else {
            job = righTbvJobs?.object(at: indexPath)
        }
        
        if let job = job {
            job.isSelected = !job.isSelected
            CoreDataManager.sharedManager.saveContext()
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        if tableView == leftTbv {
            return .insert
        } else {
            return .delete
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if tableView == leftTbv {
            let job = leftTbvJobs!.fetchedObjects![indexPath.row]
            job.isSelected = true
        } else {
            let job = righTbvJobs!.fetchedObjects![indexPath.row]
            job.isSelected = false
        }
        CoreDataManager.sharedManager.saveContext()
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        if controller == leftTbvJobs{
            leftTbv.beginUpdates()
        } else {
            rightTbv.beginUpdates()
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        if controller == leftTbvJobs {
            leftTbv.endUpdates()
        } else {
            rightTbv.endUpdates()
            sumJobsPrice()
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        let mTableView : UITableView!
        if controller == leftTbvJobs {
            mTableView = leftTbv
        } else {
            mTableView = rightTbv
        }
        
        switch type {
        case .move:
            if let indexPath = indexPath{
                mTableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let indexPath = newIndexPath{
                mTableView.insertRows(at: [indexPath], with: .fade)
            }
            break
        case .delete:
            if let indexPath = indexPath{
                mTableView.deleteRows(at: [indexPath], with: .fade)
            }
            break
        case .insert:
            if let indexPath = newIndexPath{
                mTableView.insertRows(at: [indexPath], with: .fade)
            }
            break
        case .update:
            if let indexPath = indexPath{
                let cell = mTableView.cellForRow(at: indexPath) as! MainViewTbvCell
                configureCell(cell: cell, indexPath: indexPath, tableView: mTableView)
            }
            break
        }
        
    }
    
    func sumJobsPrice(){
        var total = 0.0
        if let jobs = righTbvJobs?.fetchedObjects{
            for job in jobs {
                total += job.price
            }
        }
        
        totalValueLbl.text = "\(total),-"
    }
    
    @IBAction func resetJobs(_ sender: Any) {
        if let jobs = righTbvJobs?.fetchedObjects{
            for job in jobs {
                job.isSelected = false
            }
        CoreDataManager.sharedManager.saveContext()
        }
    }
    
}

