//
//  JobDetailTbvCell.swift
//  SuchosApp
//
//  Created by Tom Odler on 07.02.18.
//  Copyright © 2018 Tom Odler. All rights reserved.
//

import UIKit

class JobDetailTbvCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var valueTf: UITextField!
    
}
